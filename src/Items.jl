module Items

using Dates

struct Item
	title::String
	duedate::Date
	notes::String
	tags::Vector{String}
end

function Item(title::String, duedate::String)
	Item(title, Date(duedate), "", [""])
end

function Item(title::String, duedate::String, notes::String, tags::Vector{String})
	Item(title, Date(duedate), notes, tags)
end

function Item(title::String, duedate::String; dateformat::String)
	Item(title, Date(duedate, dateformat), "", [""])
end

function Item(
		title::String,
		duedate::String,
		notes::String="",
		tags::Vector{String}=[""];
		dateformat::String="yyyy-mm-dd"
		)
	Item(title, Date(duedate, dateformat), notes, tags)
end

end # module
