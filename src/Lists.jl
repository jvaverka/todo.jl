module Lists

include("Items.jl")
using .Items
using Dates

# const TODOS = joinpath(@__DIR__, "..", "data", "todos.csv")

mutable struct List
	name::String
	items::Vector{Items.Item}
end

List(name::String=Dates.now()) = List(name, Items.Item[])

newlist(name::String=string(Dates.now())) = List(name)

function addtodo(
		list::List,
		name::String,
		due::String,
		note::String,
		tags::Vector{String}
		)
	push!(list.items, Items.Item(name, due, note, tags))
end

function addtodo(list::List, name::String, due::String, dateformat::String)
	push!(list.items, Items.Item(name, due, dateformat))
end

function addtodo(list::List, name::String, due::String)
	push!(list.items, Items.Item(name, due))
end

function addtodo(
		list::List,
		name::String,
		due::String,
		note::String,
		tags::Vector{String};
		dateformat::String="yyyy-mm-dd"
		)
	push!(list.items, Items.Item(name, due, note, tags; dateformat))
end

end # module