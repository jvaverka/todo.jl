include(joinpath(@__DIR__, "..", "src", "Lists.jl"))
using .Lists
using Test

@testset "Default List creation" begin
    default_list = Lists.newlist()
    @test default_list isa Lists.List
end

@testset "Named List Creation" begin
    named_list = Lists.newlist("scratch_list")
    @test named_list isa Lists.List
    @test named_list.name == "scratch_list"
    @test count(named_list.items) == 0
end

@testset "Adding to Named List" begin
    Lists.addtodo(named_list, "brainstorm", "2022-06-21")
    @test count(named_list.items) == 1
end