using Test

@testset "Items" begin
	include("items-tests.jl")
end

@testset "Lists" begin
	include("lists-tests.jl")
end