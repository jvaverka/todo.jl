include(joinpath(@__DIR__, "..", "src", "Items.jl"))
using .Items
using Dates
using Test

full = Items.Item("full item", "2022-04-01", "important note", ["thing"])

@testset "Full Item" begin
		@test full.title == "full item"
		@test typeof(full.duedate) <: TimeType
		@test month(full.duedate) == 4
		@test !isempty(full.notes)
		@test length(full.tags) == 1
end

partial = Items.Item("partial item", "2022-04-01")

@testset "Partial Item" begin
		@test partial.title == "partial item"
		@test typeof(partial.duedate) <: TimeType
		@test month(partial.duedate) == 4
		@test isempty(partial.notes)
		@test length(partial.tags) == 1
end

partialf = Items.Item("partial formatted item", "2022-04-01"; dateformat="yyy-mm-dd")

@testset "Partial Formatted Item" begin
		@test partialf.title == "partial formatted item"
		@test typeof(partialf.duedate) <: TimeType
		@test month(partialf.duedate) == 4
		@test isempty(partialf.notes)
		@test length(partialf.tags) == 1
end

